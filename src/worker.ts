import {
  error,      // creates error responses
  json,       // creates JSON responses
  Router,     // the ~440 byte router itself
  withParams, // middleware: puts params directly on the Request
} from 'itty-router'
import Client from './client';

// create a new Router
const router = Router()

router
  // add some middleware upstream on all routes
  .all('*', withParams)
	.post('completion', async (request: Request, client: Client) =>
		client.completion(await request.json())
	)
	.post('completionStream', async (request: Request, client: Client) =>
		client.completionStream(await request.json())
	)
  // 404 for everything else
  .all('*', () => error(404))

export interface Env {
	TOKEN: string
}

export default {
	async fetch(request: Request, env: Env, ctx: ExecutionContext): Promise<Response> {
		const client = new Client(env.TOKEN)
		return router.handle(request, client, ctx)
			.catch(error)
	},
};
