import { ChatCompletionRequestMessage, Configuration, OpenAIApi } from "openai-edge"

export default class Client {
  private readonly client: OpenAIApi

  constructor(token: string) {
    const config = new Configuration({
      apiKey: token,
    })
    this.client = new OpenAIApi(config)
  }

  completion({model = 'gpt4', messages, maxTokens = 10, temperature = 0.7}: {model: string, messages: ChatCompletionRequestMessage[], maxTokens: number, temperature: number}) {
    return this.client.createChatCompletion({
      model,
      messages,
      max_tokens: maxTokens,
      temperature,
      stream: false,
    })
  }

  async completionStream({model, messages, maxTokens = 10, temperature = 0.7}: {model: string, messages: ChatCompletionRequestMessage[], maxTokens: number, temperature: number}) {
    try {
      const completion = await this.client.createChatCompletion({
        model,
        messages,
        max_tokens: maxTokens,
        temperature,
        stream: true,
      })
  
      return new Response(completion.body, {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "text/event-stream;charset=utf-8",
          "Cache-Control": "no-cache, no-transform",
          "X-Accel-Buffering": "no",
        },
      })
    } catch (error: any) {
      console.error(error)
  
      return new Response(JSON.stringify(error), {
        status: 400,
        headers: {
          "content-type": "application/json",
        },
      })
    }
  }
}